#ifndef FL_GOBOX_H
#define FL_GOBOX_H
#include "wincommon.h"
#include "histogram.hpp"
using namespace std;
using namespace cv;
class Fl_goBox : public Fl_Box
{
    public:
        Fl_goBox(int, int ,int ,int, const char*);
        virtual ~Fl_goBox();
        static void Timer_CB(void *userdata);
        void mainloop();
        void draw();
        vector<histogram> pool4;
        vector<p2i> pool3;
        string fpath;
        Mat raw;
        Mat out;
        Mat * model;
        void shrinkflow(Mat &image);

    protected:
    private:


};

#endif // FL_GOBOX_H
