#ifndef HISTOGRAM_HPP
#define HISTOGRAM_HPP
#include <iostream>
#include <exception>
#include <vector>
#include <math.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#define PI 3.14159265
#define fumeHist histogram
using namespace std;
class p2i{
public:
    int x;
    int y;
public:
    p2i();
    p2i(int _x,int  _y){x =_x;y=_y;};
    double distance(p2i &op2i){
        double res = sqrt(pow(x-op2i.x,2)+pow(y-op2i.y,2));
        return res;
    }
    double logdis(p2i &op2i){
        double res = log(distance(op2i))/log(2);
        if(res<0)
            return 0;
        return res;
    }
    p2i& operator=(const p2i & op2i){
        x=op2i.x;
        y=op2i.y;
        return *this;
    }
};
double atg360(p2i &center, p2i &arrow);
int     atg360i(p2i &center, p2i &arrow);




class histogram{
protected:
    int innerbase;
    int innerfrom;
    int innerto;
    int innerlevelfrom;
    int innerlevelto;
    vector<int*> base_data;
public:
    histogram();
    histogram(int from, int to, int level = 1){
        innerlevelfrom = 0;
        innerlevelto = level-1;
        if(from >= to){
            throw " From should less than to. ";
        }
        else if(level <= 1){
            throw " Level should bigger then 1 ";
        }
        innerfrom = 0;
        innerbase = from;
        innerto = to-from;

        for(int i = 0 ; i<=innerto ; i++){
            int * arrayi = new int[level];
            for(int w=0; w<level;w++){
                arrayi[w] = 0;
            }
            base_data.push_back(arrayi);
        }
    }
    ~histogram(){
        for(size_t i =0 ; i< base_data.size(); i++){
            delete[] base_data[i];
        }
    };
    int& operator()(const int base, const int level){
        return (base_data[base-innerbase])[level];
    }

    int& getfrom(const int base, const int level){
        return (base_data[base-innerbase])[level];
    }

    int check_total_vote(){
        int total =0;
        for(size_t i=0;i<base_data.size();i++){
            for(int w=0; w<=innerlevelto ;w++){
                total = (base_data[i])[w]+total;
            }

        }
        return total;
    }

    double compare(histogram &hist){
        if(hist.innerlevelto!=innerlevelto||hist.innerto!=innerto){
            throw " Can not compare these two histogram ";
        }
        double cost = 0;
        for(size_t i =0 ; i<base_data.size();i++){
            for(int w=0; w<=innerlevelto;w++){
               double c = (base_data[i])[w]-(hist.base_data[i])[w];
               double c1= (base_data[i])[w]+(hist.base_data[i])[w];
               if(std::abs(c1) >1){
                cost += pow(c,2)/c1;
               }
            }
        }
        return cost/2;
    }

    void show(){
        for(size_t i =0 ; i<base_data.size();i++){
            for(int w=0; w<=innerlevelto;w++){
                    std::cout << base_data[i][w]<<" ";
            }
            std::cout <<std::endl;
        }
    }
};

class logPolar:public histogram{


public:
    logPolar();
    logPolar(int slice, int level):histogram(1,slice,level){}
    void build(vector<p2i> &vec,int index){
        double maxdistance = 0;
        for(std::size_t i=0; i<vec.size();i++){
            double loca = vec[index].logdis(vec[i]);
            if(loca>maxdistance)
                maxdistance = loca;
        }
        //cout << "max "<< maxdistance <<endl;;
        double piece = abs(1/(innerlevelto+1));
        for(std::size_t i=0; i<vec.size();i++){
            if(i==index)
                continue;
            double loca = vec[i].logdis(vec[i]);
            double percent =abs(loca/maxdistance);
            int levelcount =ceil(percent*(innerlevelto+1));
            int basecount = ceil((innerto+1) * atg360i(vec[index],vec[i])/360);
            if(basecount>innerto){basecount--;}
            if(basecount>innerto||levelcount>innerlevelto){
               cout << "error0" <<endl;
            }
            if(basecount<0){
               cout << "error1" <<endl;
            }
            if(levelcount<0){
               cout << "error2" << levelcount<<endl;
            }
            base_data[basecount][levelcount]+=1;
        }


    }
    ~logPolar(){}

};

#endif // HISTOGRAM_HPP_INCLUDED
