#ifndef FEATUREDETECT_H
#define FEATUREDETECT_H

#include "wincommon.h"

#include "histogram.hpp"

using namespace cv;
class featureDetect
{
    public:
        featureDetect();
        virtual ~featureDetect();
        static Mat localMax(Mat input, int step, int thresh );
        static void nonMaximumsuppression(Mat &xy);
    protected:
    private:
};

#endif // FEATUREDETECT_H
