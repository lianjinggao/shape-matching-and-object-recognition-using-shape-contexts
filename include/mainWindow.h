#pragma once
#include "keyEventWindow.h"
#include "wincommon.h"
#include "Fl_goBox.h"
class mainWindow :
	public keyEventWindow
{
public:
    string fpath;
	mainWindow(int, int, const char *);
	~mainWindow();
	int handle_key(int e, int key);
	void compare();

private:
	Fl_Menu_Bar * menu;
    Fl_goBox * box;
    Fl_goBox * box2;
	static void open(Fl_Widget* widget, void * data);
	void opencb();
	void opencb2();
	static void cam (Fl_Widget* widget, void * data);
	static void help(Fl_Widget* widget, void * data);

	void read(const char *);

};

