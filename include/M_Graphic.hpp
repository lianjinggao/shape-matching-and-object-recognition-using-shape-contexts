#ifndef M_GRAPHIC_HPP_INCLUDED
#define M_GRAPHIC_HPP_INCLUDED
//
//  Convert from A55
//
//  Created by LianjingGao on 4/17/14.
//  Copyright (c) 2014 LianjingGao. All rights reserved.
//
#include <iostream>
#include <vector>
#include <stack>
using namespace std;

class M_edge;
class M_node;

class M_edge {
public:
    bool mark;
    double len;
    M_node * from;
    M_node * to;
    M_edge(M_node &sour, M_node &dest){
        mark = false;
        len = 0;
        from = &sour;
        to = &dest;
    }
    M_edge(M_node *sour, M_node *dest){
        mark = false;
        len = 0;
        from = sour;
        to = dest;
    }
    M_edge(M_node *sour, M_node *dest, double length){
        mark = false;
        len = length;
        from = sour;
        to = dest;
    }
    void reverseDirectionValue(){
        len = len*(-1);
        M_node * temp = from;
        from = to;
        to = temp;
    }
    double length(){
        return len;
    }
    ~M_edge();

};



class M_node {
public:
    bool mark;
    int index;
    vector<M_node *> adjn;//node and edge are corresponding
    vector<M_edge *> adje;
    M_node(){mark =false;}
    M_node(int indexN){
        index = indexN;
        mark = false;
    };
    /*
    void addAjacentNode(M_node* addN){
        adjn.push_back(addN);
    }
    void addAjacentNode(M_node& addN){
        adjn.push_back(&addN);
    }*/
    void addAjacentEdge(M_edge* addE){
        adje.push_back(addE);
        adjn.push_back(addE->to);
    }
    void addAjacentEdge(M_edge& addE){
        adje.push_back(&addE);
        adjn.push_back(addE.to);
    }
    M_edge* getEdgeWith(M_node* to){
        int pos = find(adjn.begin(), adjn.end(), to) - adjn.begin();
        return adje[pos];
    }
    ~M_node(){
        adjn.clear();
    }

};

class M_path{

public:
    M_path(M_node* origin){nodechain.push_back(origin);}
    ~M_path(){};
    vector<M_node*> nodechain;
    vector<M_edge*> edgechain;
    void addOrigin(M_node* node){
        nodechain.push_back(node);
    }
    void addNode(M_node* node){
        nodechain.push_back(node);
        M_node * prev = nodechain.back();
        edgechain.push_back(prev->getEdgeWith(node));
    }
    void pop(){
        if(nodechain.size()>1){
            nodechain.pop_back();
            edgechain.pop_back();
        }
        else{
            nodechain.pop_back();
        }
    }
    M_node * top(){
        return nodechain.back();
    }
    void reverseDirectionValue(){
        if(!edgechain.size()>1)return;
        for(unsigned int i = 0; i < edgechain.size()-1; i++){
            M_node* curr = nodechain[i];
            for(unsigned int w =0 ; w<curr->adjn.size();w++){
                if(curr->adjn[w]==nodechain[i+1]&&curr->adje[w]==edgechain[i]){//This code is very likely to be wrong
                    curr->adjn.erase(curr->adjn.begin()+w-1);
                    curr->adje.erase(curr->adje.begin()+w-1);
                    break;
                }
            }
            M_node* next = nodechain[i+1];
            M_edge* currE= edgechain[i];
            currE->reverseDirectionValue();
            next->addAjacentEdge(currE);
        }
    }
    double length(){
        double length = 0;
        for(unsigned int i=0; i<edgechain.size()-1;i++){
            length+= edgechain[i]->length();
        }
        return length;
    }
    bool empty(){
        if(nodechain.size()==0){
            return true;
        }
        return false;
    }
};


void BFS(M_node *  sour, M_node * dest){
    for (unsigned int i= 0; i< sour->adjn.size(); i++) {
        if (sour->adjn[i]==dest) {
            return;
        }
        else{
            BFS(sour->adjn[i],dest);
        }
    }
}


void BFS_clean(){




}

void DFS(M_node *  sour, M_node * dest){
    stack<M_node *> record;
    record.push(sour);
    while (record.size()>0) {
        M_node * curr = record.top();
        curr->mark=true;
        if(curr->adjn.size()>0){
            for (unsigned int i= 0; i< curr->adjn.size(); i++) {
                if (curr->adjn[i]==dest) {
                    record.push(dest);//If we find destination we can return and do cleanning now.
                    goto DFSEntry1;
                }
                else if(curr->adjn[i]->mark!=true){
                    record.push(curr->adjn[i]);//find an unvisited child, we search it immediately next loop...
                    goto DFSEntry0;
                }
            }
            record.pop();//when all neighbour is not destination and all have visited, we revisit it's parent
        }
        else{
            record.pop();//when this nod is not destination and have no child, we revisit it's parent
        }
    DFSEntry0:
        continue;
    }
    return;//If algorithm reached here that means no path from sour to dest exited.
DFSEntry1:

    int x = (int)record.size();
    int i=1;
    while (i<=x) {
        cout << record.top()->index;
        record.pop();
        i++;
    }

    return;
}


class M_Bmap {
public:
    vector<M_node *> set1;
    vector<M_node *> set2;
    vector<M_edge *> edges;
    M_node * ps;
    M_node * pd;
    M_node source;
    M_node destin;
    M_Bmap(){}
    ~M_Bmap(){}
    void mapInitialCheck();
    void buildMaxFlowMap();
    void findMaxAssignment();
    bool findAugmentPath(M_node *  sour, M_node * dest);
};

void M_Bmap::mapInitialCheck(){
    for(unsigned int i=0; i<edges.size();i++){
        M_node * cufrom =edges[i]->from;
        M_node * cuto   =edges[i]->to;
        vector<M_node*>::iterator it = find(cufrom->adjn.begin(),cufrom->adjn.end(),cuto);
        if(it==cufrom->adjn.end()){
            cufrom->addAjacentEdge(edges[i]);
        }
    }
}

void M_Bmap::buildMaxFlowMap(){
    source = M_node();
    destin = M_node();
    source.adjn = set1;//need to beware
    ps = &source;
    pd = &destin;
    for(unsigned int i=0; i<set1.size();i++){
        M_edge * tempEdge = new M_edge(ps,  set1[i],0);
        edges.push_back(tempEdge);
    }
    for(unsigned int i=0; i<set2.size();i++){
        M_edge * tempEdge = new M_edge(set2[i], pd, 0);
        edges.push_back(tempEdge);
    }

}

void M_Bmap::findMaxAssignment(){
    if(ps == NULL){
        buildMaxFlowMap();
    }


}
/*
bool M_Bmap::findAugmentPath(M_node *  sour, M_node * dest){
    M_path path = M_path(sour);
    while (!path.empty()) {
        M_node * curr = path.top();
        curr->mark=true;
        if(curr->adjn.size()>0){
            for (unsigned int i= 0; i< curr->adjn.size(); i++) {
                if (curr->adjn[i]==dest) {
                    record.push_back(dest);//If we find destination we can return and do cleanning now.
                    goto findAugmentPath0;
                }
                else if(curr->adjn[i]->mark!=true){
                    record.push_back(curr->adjn[i]);//find an unvisited child, we search it immediately next loop...
                    goto findAugmentPath1;
                }
            }
            record.pop_back();//when all neighbour is not destination and all have visited, we revisit it's parent
        }
        else{
            path.pop();//when this nod is not destination and have no child, we revisit it's parent
        }
    findAugmentPath0:
        continue;
    }
    return false;
findAugmentPath1:
    return true;
}*/
#endif // M_GRAPHIC_HPP_INCLUDED
