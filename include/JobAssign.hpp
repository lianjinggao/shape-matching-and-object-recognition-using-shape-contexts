#ifndef JOBASSIGN_HPP_INCLUDED
#define JOBASSIGN_HPP_INCLUDED
#include <iostream>
#include <fstream>
class Job_Assign{
public:
int** costM;
int** storeM;
int** maskM;
int* RowCover;
int* ColCover;
int nrow;
int mcol;
int step;
int path_row_0;
int path_col_0;
int path_count;
int** path;

public:
    Job_Assign(){step=1;path_count =0;}
    ~Job_Assign(){};
public:
/*
void step1();
void step2();
void step3();
void step4();
    void find_a_zero(int &row,int &col);
    bool star_in_row(int row);
    void find_star_in_row(int row, int &col);
void step5();
    void find_star_in_col(int c, int &r);
    void find_prime_in_row(int r, int &c);
    void augment_path();
    void clear_covers();
    void erase_primes();
void step6();
    void find_smallest(int &minval);
void step7();
*/
void step1(){
    int min_in_row;
    for (int r = 0; r < nrow; r++){
        min_in_row = costM[r][0];
        for (int c = 0; c < mcol; c++)
            if (costM[r][c] < min_in_row)
                min_in_row = costM[r][c];
        for (int c = 0; c < mcol; c++)
            costM[r][c] -= min_in_row;
        }
    step = 2;
}

void step2(){
    for(int r= 0 ;r < nrow;r++){
        for(int c=0; c<mcol;c++){
            if(costM[r][c]==0&&RowCover[r]==0&&ColCover[c]==0){
                maskM[r][c]=1;
                RowCover[r]=1;
                ColCover[c]=1;
            }
        }
    }

    for (int r = 0; r < nrow; r++)
        RowCover[r] = 0;
    for (int c = 0; c < mcol; c++)
        ColCover[c] = 0;
    step = 3;
}

void step3(){
    int colcount;
    for(int r= 0 ;r < nrow;r++){
        for(int c=0; c<mcol;c++){
            if(maskM[r][c]==1){
                ColCover[c]=1;
            }
        }
    }
    colcount=0;
    for(int c=0; c<mcol;c++){
        if(ColCover[c]==1){
            colcount +=1;
        }
    }
    if(colcount >=mcol||colcount>=nrow)
        step = 7;
    else
        step = 4;
}

void step4(){
    int row = -1;
    int col = -1;
    bool done= false;
    while(!done){
        find_a_zero(row,col);
        if(row==-1){
            done = true;
            step = 6;
        }
        else{
            maskM[row][col]=2;
            if(star_in_row(row)){
                find_star_in_row(row,col);
                RowCover[row]=1;
                ColCover[col]=0;
            }
            else{
                done = true;
                step = 5;
                path_row_0 = row;
                path_col_0 = col;
            }

        }
    }
}

void find_a_zero(int &row, int &col){
    int r =0;
    int c;
    bool done = false;
    row = -1;
    col = -1;
    while(!done){
        c= 0;
        while(true){
            if(costM[r][c]==0&&RowCover[r]==0&&ColCover[c]==0){
                row =r;
                col=c;
                done=true;
            }
            c+=1;
            if(c>=mcol||done)
                break;
        }
        r+=1;
        if(r>=nrow)
            done = true;
    }
}

bool star_in_row(int row){
    bool tmp= false;
    for(int c=0; c<mcol;c++)
        if(maskM[row][c]==1)
            tmp=true;
    return tmp;
}

void find_star_in_row(int row ,int & col){

    col = -1;
    for(int c= 0; c<mcol;c++){
        if(maskM[row][c]==1)
            col=c;
    }
}
//----------------------------------------------------------------------------5
void find_star_in_col(int c, int &r){
    r = -1;
    for (int i = 0; i < nrow; i++)
        if (maskM[i][c] == 1)
            r = i;
}

void find_prime_in_row(int r, int &c){
    for (int j = 0; j < mcol; j++)
        if (maskM[r][j] == 2)
            c = j;
}


void augment_path(){
    for (int p = 0; p < path_count; p++)
        if (maskM[path[p][0]][path[p][1]] == 1)
            maskM[path[p][0]][path[p][1]] = 0;
        else
            maskM[path[p][0]][path[p][1]] = 1;
}

void clear_covers(){
    for (int r = 0; r < nrow; r++)
        RowCover[r] = 0;
    for (int c = 0; c < mcol; c++)
        ColCover[c] = 0;
}

void erase_primes(){
    for (int r = 0; r < nrow; r++)
        for (int c = 0; c < mcol; c++)
            if (maskM[r][c] == 2)
                maskM[r][c] = 0;
}


void step5(){
    bool done = false;
    int r = -1;
    int c = -1;
    path_count = 1;
    path[path_count - 1][0] = path_row_0;
    path[path_count - 1][1] = path_col_0;
    while (!done){
        find_star_in_col(path[path_count - 1][1], r);
        if (r > -1)
        {
            path_count += 1;
            path[path_count - 1][0] = r;
            path[path_count - 1][1] = path[path_count - 2][1];
        }
        else{
            done = true;
        }
        if (!done)
        {
            find_prime_in_row(path[path_count - 1][0], c);
            path_count += 1;
            path[path_count - 1][0] = path[path_count - 2][0];
            path[path_count - 1][1] = c;
        }
    }
    augment_path();
    clear_covers();
    erase_primes();
    step = 3;
}

void find_smallest(int &minval){
    for (int r = 0; r < nrow; r++){
        for (int c = 0; c < mcol; c++){
            if (RowCover[r] == 0 && ColCover[c] == 0){
                if (minval > costM[r][c]){
                    minval = costM[r][c];
                }
            }
        }
    }
}

void step6(){
    int minval = 800000;
    find_smallest(minval);
    for (int r = 0; r < nrow; r++){
        for (int c = 0; c < mcol; c++){
            if (RowCover[r] == 1){
                costM[r][c] += minval;
            }
            if (ColCover[c] == 0){
                costM[r][c] -= minval;
            }
        }
    }
    step = 4;
}

void step7(){
    cout << "Cost Matrix ! =" <<endl;
    for(int r= 0 ;r < nrow;r++){
        cout <<endl;
        for(int c=0; c<mcol;c++){
            cout << costM[r][c]<< " ";
        }
    }
    cout <<endl;
    cout << "Mask Matrix ! =" <<endl;
    for(int r= 0 ;r < nrow;r++){
        cout <<endl;
        for(int c=0; c<mcol;c++){
            cout << maskM[r][c] << " ";
        }
    }
}

void step8(){
    int cost = 0;
    for(int r= 0 ;r < nrow;r++){
        for(int c=0; c<mcol;c++){
            if(maskM[r][c]==1){
                cost+=storeM[r][c];
            }
        }
    }
    cout <<"totoal cost :" <<cost <<endl;

}

void make(int n,int m){
    nrow =n;
    mcol =m;
    costM = new int*[n];
    storeM = new int*[n];
    for(int i = 0; i < n; ++i){
        costM[i] = new int[m];
        storeM[i] = new int[m];
        for(int w=0; w< m;w++){
           costM[i][w] = 0;
           storeM[i][w] =0;
        }
    }
    maskM = new int*[n];
    for(int i = 0; i < n; ++i){
        maskM[i] = new int[m];
        for(int w=0; w< m;w++)
           maskM[i][w] = 0;
    }

    path  = new int*[n+m+1];
    for(int i = 0; i < n+m+1; ++i){
        path[i] = new int[2];
        memset (path[i],0,2*sizeof(int));
    }

    RowCover = new int[n];
    for(int w=0; w< n;w++)
           RowCover[w] = 0;
    ColCover = new int[m];
    for(int w=0; w< m;w++)
           ColCover[w] = 0;
}

void run(){
    bool done =false;

    for(int r= 0 ;r < nrow;r++){
        for(int c=0; c<mcol;c++){
            storeM[r][c]=costM[r][c];
        }
    }


    while(!done){
        switch(step){
        case 1:
            step1();
            break;
        case 2:
            step2();
            break;
        case 3:
            step3();
            break;
        case 4:
            step4();
            break;
        case 5:
            step5();
            break;
        case 6:
            step6();
            break;
        case 7:
            //step7();
            //step8();
            done = true;
            break;
        }
    }
    //cout << "yes" <<endl;
}


void output(){

    ofstream myfile;
    myfile.open ("example.txt");
    for(int r= 0 ;r < nrow;r++){
        for(int c=0; c<mcol;c++){
            myfile << costM[r][c] << " ";
        }
        myfile << "\n";
    }
    myfile.close();

}

};

#endif // JOBASSIGN_HPP_INCLUDED
