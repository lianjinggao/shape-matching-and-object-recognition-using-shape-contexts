#include "mainWindow.h"
#include "JobAssign.hpp"
using namespace std;
using namespace cv;

mainWindow::mainWindow(int w, int h ,const char * title):keyEventWindow(w, h, title)
{
	//cout << "mainwindow add:" << this << endl;
	begin();
		menu =new Fl_Menu_Bar(0, 0, this->w(), 25);
		menu->add("&File/&Open", 0, open,(void*)this);
		menu->add("&File/&Open2", 0, cam,(void*)this);
		menu->add("&Help/&Description", 0, help);
        box = new Fl_goBox(0,25,800,600," ");
        box2 = new Fl_goBox(800,25,800,600," ");

        //string temp("D:\\misc\\A2.jpg");
        //box->fpath = temp;
        //string temp2("D:\\misc\\C2.jpg");
        //box2->fpath = temp2;

	end();
}

mainWindow::~mainWindow(){

}

void mainWindow::opencb(){
    Fl_File_Chooser chooser(".",                        // directory
                            "*",                        // filter
                            1,     // chooser type
                            "Choose a file");        // title
    chooser.show();
    while(chooser.shown())
        { Fl::wait(); }
    if ( chooser.value() == NULL ){return;}
    string temp(chooser.value());
    box->fpath = temp;
}

void mainWindow::opencb2(){
    Fl_File_Chooser chooser(".",                        // directory
                            "*",                        // filter
                            1,     // chooser type
                            "Choose a file");        // title
    chooser.show();
    while(chooser.shown())
        { Fl::wait(); }
    if ( chooser.value() == NULL ){return;}
    string temp(chooser.value());
    box2->fpath = temp;

}

void mainWindow::open(Fl_Widget* widget, void * data){
	((mainWindow*)data)->opencb();
}

void mainWindow::cam(Fl_Widget* widget, void * data){
    ((mainWindow*)data)->opencb2();
}

void mainWindow::help(Fl_Widget* widget, void * data){

    fl_alert("After choose 2 images, press c to compare" );

}

void mainWindow::compare(){
    cout <<" do compare "<<endl;

    Job_Assign ass1 = Job_Assign();
    ass1.make((int)box->pool3.size(),(int)box2->pool3.size());
    int counts =0;
    for(unsigned int i=0; i<ass1.nrow ;i++){
        for(unsigned int w =0 ; w<ass1.mcol;w++){
            ass1.costM[i][w]=(int)(box->pool4[i].compare(box2->pool4[w]));
            /*
            if(ass1.costM[i][w]==0){
                cout <<"i: "<< i<< "w: " << w<<endl;
            }
            if(i==w){
                cout << box->pool3[i].x <<" " <<box->pool3[i].y <<endl;
                cout << "hist" <<endl;
                box->pool4[i].show();
                cout << box2->pool3[w].x <<" " <<box2->pool3[w].y <<endl;
                cout << "hist" <<endl;
                box2->pool4[w].show();
                cout <<"i=w "<< (int)(box->pool4[i].compare(box2->pool4[w]))<<endl;
            }
            */
        }
    }
    ass1.output();
    ass1.run();

    IplImage* output = cvCreateImage(box->raw.size(),8,3);
    cvZero(output);

    for(unsigned int i =0; i<box->pool3.size();i++){        cvCircle(output, cvPoint((box->pool3[i]).x,(box->pool3[i]).y), 2, CV_RGB(0,255,0), 1, 8);
        cvCircle(output, cvPoint((box2->pool3[i]).x,(box2->pool3[i]).y), 2, CV_RGB(255,255,0), 1, 8);
    }
     for(unsigned int i =0; i<ass1.nrow;i++){
        for(unsigned int w =0;w< ass1.mcol; w++){
            if(ass1.maskM[i][w]==1){
                cvLine(output, cvPoint((box->pool3[i]).x,(box->pool3[i]).y), cvPoint((box2->pool3[w]).x,(box2->pool3[w]).y), CV_RGB(0,0,255), 1,8,0);
                break;
            }
        }
    }
    cvNamedWindow("Output", CV_WINDOW_AUTOSIZE);
    cvShowImage("Output", output);
    ass1.step8();

}


int mainWindow::handle_key(int e, int key){
	if ('a' <= key&&key <= 'z'&&Fl::event_shift()){
		char c = key - 32;
		std::cout << c << std::endl;
	}
	else
	{
		char c = key;
		if(key =='c'){
            compare();
		}
		//std::cout << c << std::endl;
	}
	return 1;
}
