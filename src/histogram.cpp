#include "histogram.hpp"

double atg360(p2i &center, p2i &arrow){
    double temp = atan2((arrow.y-center.y),(arrow.x-center.x))*180/PI;
    if(temp<0){
        temp+=360;
    }
    return temp;
}

int atg360i(p2i &center, p2i &arrow){
    double temp = atan2((arrow.y-center.y),(arrow.x-center.x))*180/PI;
    if(temp<0){
        temp+=360;
    }
    return (int)temp;
}
