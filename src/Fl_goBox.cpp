#include "Fl_goBox.h"

using namespace std;
using namespace cv;
Fl_goBox::Fl_goBox(int x, int y, int w, int h,const char* l) :Fl_Box(x,y,w,h,l)
{
    model=NULL;
    Fl::add_timeout(1, Timer_CB,(void*) this);
}

Fl_goBox::~Fl_goBox()
{

}

void Fl_goBox::Timer_CB(void *userdata) {
        Fl_goBox *pb = (Fl_goBox*)userdata;
        pb->redraw();
        Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
}

void Fl_goBox::draw(){
    mainloop();
	if (!out.empty()){
        //if(out.cols!=800||out.rows!=600){
           // cv::resize(out, out, Size(800,600));
        //}
		if (out.channels() == 3){
			fl_draw_image(out.datastart, this->x(), this->y(), out.cols,  out.rows, 3, 0);
		}
		if (out.channels() == 1){
			fl_draw_image_mono(out.datastart, this->x(), this->y(),  out.cols,  out.rows);
		}
	}
}

void Fl_goBox::mainloop(){
    if(fpath.empty()){
    }
    else{
        vector<p2i> pool2;
        pool3.clear();
        raw = imread(fpath.c_str());
        cvtColor( raw, raw, CV_BGR2GRAY );

        cv::resize(raw, raw, cv::Size(50,50));

        out = raw.clone();
        blur( raw, raw, Size(3,3) );
        Canny(raw, out, 10, 100 );
        int num =0;
        for(int r=0 ; r<out.rows;r++){
            for(int w =0 ; w< out.cols;w++){
                if(out.at<uchar>(r,w)>=255){
                    p2i point(w,r);
                    pool2.push_back(point);
                    num++;
                }
            }
        }
        std::random_shuffle ( pool2.begin(), pool2.end()  );
        for(unsigned int i=0;i<pool2.size();i++){
            pool3.push_back(pool2[i]);
        }

        for(size_t i=0; i<pool3.size();i++){
            circle(out, Point2i(pool3[i].x,pool3[i].y), 2, Scalar(255,255,255));
        }
        for(unsigned int i=0; i<pool3.size();i++){
            logPolar hist(12,5);
            hist.build(pool3,i);
            pool4.push_back(hist);
        }
        fpath.clear();
    }

}
