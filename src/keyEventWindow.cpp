#include "keyEventWindow.h"

keyEventWindow::keyEventWindow(int w, int h, const char * title) :Fl_Window(w, h, title)
{
}


keyEventWindow::~keyEventWindow()
{
}

int
keyEventWindow::handle(int e){
	switch (e){
	case FL_KEYDOWN:
		handle_key(e, Fl::event_key());
		return 1;
	default:
		return Fl_Window::handle(e);
	};
}


int
keyEventWindow::handle_key(int e, int key){

	if ('a'<=key&&key<='z'&&Fl::event_shift()){
		char c = key-32;
		std::cout << c << std::endl;
	}
	else
	{
		char c = key;
		std::cout << c << std::endl;
	}

	return 1;
}
